import {
  ChangeDetectionStrategy,
  Component, ElementRef, HostListener,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgxSelectComponent} from "ngx-select-ex";
import * as data from '../data.json'; // fake data
import {
  typeAffiliateItems,
  typeBranches,
  typeCurrencies,
  typeCustomers, typeDiscountType, typeInvoiceTypes, typeOrders,
  typeRepositories,
  typeServices, typeUnits
} from '../basics';

@Component({
  selector: 'app-form-array',
  templateUrl: './form-array.component.html',
  styleUrls: ['./form-array.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // handle push by ChangeDetectionStrategy from angular core
})
export class FormArrayComponent implements OnInit {
  @ViewChildren('goods_services') ngxSelectComponent: QueryList<NgxSelectComponent>;
  @ViewChildren('quantities') quantities: QueryList<ElementRef>;
  // @ViewChildren('rows') rows: QueryList<ElementRef>;

  form: FormGroup;
  getData: any = (data as any).default;

  services: typeServices[] = [];
  units: typeUnits[][] = [];
  branches: typeBranches[] = [];
  invoiceTypes: typeInvoiceTypes[] = [];
  repositories: typeRepositories[] = [];
  affiliateItems: typeAffiliateItems[] = [];
  customers: typeCustomers[] = [];
  currencies: typeCurrencies[] = [];
  orders: typeOrders[] = [];
  discountType: typeDiscountType[] = [
    {id: 1, name: "قيمة الخصم"},
    {id: 2, name: "%"}
  ];

  fieldArraysForm: typeServices[] = [];
  current_val_Goods_services: string[] = [""];
  goodsServicesToggle: boolean[] = [true];
  total = 0;

  constructor(private formBuilder: FormBuilder) {
    this.services = this.getData.services;
    this.currencies = this.getData.currencies;
    this.repositories = this.getData.repositories;
    this.branches = this.getData.branches;
    this.customers = this.getData.customers;
    this.invoiceTypes = this.getData.invoiceTypes;
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      /*date: ['', Validators.required],
      branch_id: ['', Validators.required],
      repository_id: ['', Validators.required],
      currency_id: ['', Validators.required],
      order_id: ['', Validators.required],
      customer_id: '',
      invoice_type_id: '',
      reference_code: '',
      automatic_exchange: false,
      affiliate_item_id: '',*/
      services: this.formBuilder.array([])
    })

    // Create a new default row
    this.appendFormArray();
  }

  get isInValidFormArray() {
    return this.form.get('services') as FormArray;
  }

  submit(value: object[]): void {
    console.log(value)
  }

  addNewRow(): void {
    // 13 key enter
    this.appendFormArray();
    setTimeout(() => { // Delay until initialization is done
      this.ngxSelectComponent.last.optionsOpen(); // Opening options with new row
    })
  }

  appendFormArray(): void {
    const control = this.form.get('services') as FormArray;
    control.push(this.patchValuesInit());
    this.fieldArraysForm.push({});
  }

  removeFormArray(index: number): void {
    const control = this.form.get('services') as FormArray;
    control.removeAt(index);
    this.fieldArraysForm.splice(index, 1);
    this.goodsServicesToggle.splice(index, 1);
    this.current_val_Goods_services.splice(index, 1);
    this.units.splice(index, 1);
    if (control.length === 0) {
      this.appendFormArray();
    }
    this.calculate(); // Recalculate after deletion
  }

  patchValuesInit() {
    return this.formBuilder.group({
      discount_type_id: 2,
      goods_service_id: '',
      unit_id: '',
      quantity: [1, Validators.required],
      unit_price: [0, Validators.required],
      discount: 0,
      taxable_amount: 0,
      tax_rate: 0,
      tax_amount: 0,
      gross_incl_vat: 0,
      discount_percentage: 0,
    });
  }

  goodsServicesHidden($event: string, index: number): void {
    switch ($event) {
      case 'show' :
        this.goodsServicesToggle[index] = true;
        setTimeout(() => { // Delay until initialization is done
          this.ngxSelectComponent.last.optionsOpen(); // Opening options with new row
        });
        break;
      case 'hide' :
        this.goodsServicesToggle[index] = false
        break;
    }
  }

  getGoodsAndServices($event: any, index: number) {
    const control = this.form.get('services') as FormArray;

    if ($event) {

      setTimeout(() => { // Delay until initialization is done
        this.quantities.last.nativeElement.focus(); // Auto touch on the quantity input field
      })

      const current_select = this.services.find(i => i.id == $event); // get row by id
      if (current_select) {
        this.fieldArraysForm[index] = current_select; // append current service
        this.current_val_Goods_services[index] = current_select?.name || '';
        const current_unit = current_select?.unit?.find(i => i?.id === current_select?.unit_id); // get row by id

        // set unit_id, tax_rate in form
        control.at(index).patchValue({
          unit_id: current_select?.unit_id || '',
          tax_rate: current_select?.tax_rate || 0,
          unit_price: current_unit?.price || 0,
          tax_amount: current_select?.tax_amount || 0
        });

        this.units[index] = current_select?.unit || []; // set units by service
      }
    } else {
      // If clear service field
      control.at(index).patchValue({unit_id: ""}); // set unit_id in form
      this.units[index] = []; // clear units
    }
    this.goodsServicesToggle[index] = false;
    this.calculate(); // calculate after select
  }

  calculate() {
    const control = this.form.get('services') as FormArray;
    this.total = 0;

    control?.controls?.forEach((item, index) => {
      // Actual Data by api
      const getActualData = this.fieldArraysForm[index];
      const taxable_amount = (item.get('quantity')?.value * item.get('unit_price')?.value).toFixed(2);
      let tax_amount = '0.00'
      let discount_percentage = '0.00'
      let taxable_amount_after_discount = '0.00'

      switch (item.get('discount_type_id')?.value) {
        case 1: // discount value
          // Calc (quantity x unit price) - discount (fix 2 digits)
          taxable_amount_after_discount = (parseFloat(taxable_amount) - item.get('discount')?.value).toFixed(2)
          // Calc (Tax rate x unit price x quantity) - discount (fix 2 digits)
          tax_amount = (((getActualData?.tax_rate || 0) / 100) * ((item.get('unit_price')?.value * item.get('quantity')?.value) - item.get('discount')?.value)).toFixed(2)
          // Calc percentage (discount / taxable_amount) * 100 (fix 2 digits) = %
          if (parseFloat(taxable_amount)) discount_percentage = ((item.get('discount')?.value / parseFloat(taxable_amount)) * 100).toFixed(2)
          console.log(item.get('discount')?.value, parseFloat(taxable_amount));
          break;
        case 2: // Percentage
          discount_percentage = ((item.get('discount')?.value * parseFloat(taxable_amount)) / 100).toFixed(2)
          // Calc (Taxable amount - Discount percentage) (fix 2 digits)
          taxable_amount_after_discount = (parseFloat(taxable_amount) - parseFloat(discount_percentage)).toFixed(2)
          // Calc (Tax rate x unit price x quantity) - discount (fix 2 digits)
          tax_amount = (((getActualData?.tax_rate || 0) / 100) * ((item.get('unit_price')?.value * item.get('quantity')?.value) - parseFloat(discount_percentage))).toFixed(2)
          break;
      }
      // Calc Taxable amount + Tax amount (fix 2 digits)
      const gross_incl_vat = ((parseFloat(taxable_amount_after_discount) || 0) + (parseFloat(tax_amount) || 0)).toFixed(2)

      // Add sum calculation in form
      control.at(index).patchValue({
        taxable_amount: taxable_amount_after_discount,
        tax_amount,
        gross_incl_vat,
        discount_percentage
      })
      this.total += parseFloat(gross_incl_vat)
    })

  }

  clearUnits($event: any, index: number) {
    console.log($event)
    const control = this.form.get('services') as FormArray;
    // set unit_id, tax_rate in form
    control.at(index).patchValue({
      unit_id: '',
      unit_price: 0,
      tax_amount: 0,
      gross_incl_vat: 0,
      taxable_amount: 0
    });
  }
}
