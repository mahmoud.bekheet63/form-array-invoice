import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {FormArrayComponent} from './form-array/form-array.component';
import {NgxSelectModule} from 'ngx-select-ex';
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {TooltipConfig, TooltipModule} from "ngx-bootstrap/tooltip";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FocusedRowDirective } from './focused-row.directive';

// such override allows to keep some initial values
export function getAlertConfig(): TooltipConfig {
  return Object.assign(new TooltipConfig(), {
    placement: 'top',
    container: 'body',
    delay: 500
  });
}

@NgModule({
  declarations: [
    AppComponent,
    FormArrayComponent,
    FocusedRowDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSelectModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    FormsModule
  ],
  providers: [{ provide: TooltipConfig, useFactory: getAlertConfig }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
