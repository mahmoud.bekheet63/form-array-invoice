export interface typeServices {
  id?: number;
  name?: string;
  unit_id?: number;
  tax_rate?: number;
  tax_amount?: number;
  unit?: typeUnits[]
}
export interface typeUnits {
  id: number;
  name: string;
  price: number;
}
export interface typeBranches {
  id: number;
  name: string;
}
export interface typeInvoiceTypes {
  id: number;
  name: string;
}
export interface typeCurrencies {
  id: number;
  name: string;
  symbol: string;
  rate: number;
}
export interface typeRepositories {
  id: number;
  name: string;
}
export interface typeAffiliateItems {
  id: number;
  name: string;
}
export interface typeCustomers {
  id: number;
  name: string;
}
export interface typeOrders {
  id: number;
  name: string;
}
export interface typeDiscountType {
  id: number;
  name: string;
}
